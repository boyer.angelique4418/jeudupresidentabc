# Jeu du président

## Description

Ce jeu a été développé dans le cadre du bachelor Informatique.
Le but du jeu du président est de se débarrasser le premier de toutes ses cartes.
Ce jeu se joue idéalement à 4 personnes mais il est possible de jouer jusqu’à 6.

## Comment Installer le jeu

### Prérequis

- Python 3.6 ou supérieur

### Installation

- Télécharger le projet
- Ouvrir un terminal
- Se placer dans le dossier du projet
- Exécuter la commande `python src/main.py`
- Jouer

## Auteurs

- [**Angélique Boyer**]
- [**Denis Allioux**]
- [**Estèphe Caret**]
