class Carte:
        def __init__(self, rank, color):
          # Valeur de la carte
          self.rank = rank
          # Type de la carte
          self.color = color
          # On vient chercher l'index du rang de la carte dans le tableau RANKS puis via le tableau values on récupère la valeur de la carte
          # Exemple VALET est a la position 10 dans le tableau RANKS, donc elle est égale a 10 dans le tableau VALUES
          # Exemple 2 dans ranks est a la position 1 donc il est égal a 13 dans le tableau VALUES
          self.value = VALUES()[RANKS().index(rank)]
        
        def __str__(self):
            return self.rank + " " + self.color
        def getCard(self):
            return self.rank + " " + self.color

def COLORS(): return ['coeur', 'carreau', 'trefle', 'pique']
def RANKS(): return [ "As", "2", "3", "4", "5", "6", "7","8", "9", "10", "Valet", "Dame", "Roi" ]
#Tableau des valeurs des cartes par rapport a ranks (meme index)
def VALUES(): return [13,14,2,3,4,5,6,7,8,9,10,11,12]


    
