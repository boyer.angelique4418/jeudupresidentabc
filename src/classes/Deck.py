import random
import Cartes
class Deck:
        # Création du paquet de cartes
        def __init__(self,size):
            """
            Création du paquet de cartes.

            Arguments:
                size -- le nombre de cartes dans le paquet
            """

            self.size = size
            # Tableau de cartes
            self.cards = []
            # On ajoute les cartes au tableau
            for i in range(0,size):
                self.cards.append(Cartes.Carte(Cartes.RANKS()[i%13], Cartes.COLORS()[i%4]))

        def shuffle(self):
            """
            Mélange le paquet de cartes.
            """

            random.shuffle(self.cards)
        
        def get_size(self):
            """
            Renvoie la taille du paquet de cartes.
             
            Returns:
            La taille du paquet de cartes.
            """
            
            return self.size
