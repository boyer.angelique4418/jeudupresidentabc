import random
class Player:
    def __init__(self, name=""):
        """
        Initialise une nouvelle instance de Player.

        Args:
            name: Le nom du joueur. Si aucun nom n'est fourni,
                  un nom aléatoire sera généré.
        """

        if(name == ""):
            name = "Joueur " + str(random.randint(0, 1000))
        self.name = name
        self.hand = []

    def __str__(self):
        """
        Retourne une représentation en chaîne de caractères du joueur.
        """

        return "Le Joueur : " + self.name + " a " + str(len(self.hand)) + " cartes"

    def add_to_hand(self, card):
        """
        Ajoute une carte à la main du joueur.

        Args:
            card: La carte à ajouter à la main du joueur.
        """

        self.hand.append(card)

    def remove_from_hand(self, card):
        """
        Supprime une carte de la main du joueur.

        Args:
            card: La carte à supprimer de la main du joueur.
        """
        
        self.hand.remove(card)
    
    def has_card(self, rank, color):
        """
        Vérifie si le joueur possède une carte avec le rang et la couleur spécifiés.

        Args:
            rank: Le rang de la carte à rechercher.
            color: La couleur de la carte à rechercher.

        Returns:
            True si le joueur possède une carte avec le rang et la couleur spécifiés,
            False sinon.
        """

        for card in self.hand:
            if card.rank == rank and card.color == color:
                return True
        return False

    def play(self, card):
        """
        Supprime une carte de la main du joueur et la retourne.

        Args:
            card: La carte à supprimer de la main du joueur.

        Returns:
            La carte qui a été supprimée de la main du joueur.
        """

        self.remove_from_hand(card)
        return card

    def get_hand(self):
        """
        Retourne la main du joueur.

        Returns:
            Une liste de cartes représentant la main du joueur.
        """

        return self.hand

    def getPlayableCard(self, lastCardPlayed):
        """
        Obtient les cartes que le joueur peut jouer.

        Args:
            lastCardPlayed: La dernière carte jouée dans la partie.

        Returns:
            Une liste de cartes que le joueur peut jouer.
        """

        if(lastCardPlayed == "Nouveau Tour"):
            return self.hand
        higherCards = []
        for card in self.get_hand():
            if (card.value >= lastCardPlayed.value):
                higherCards.append(card)
        return higherCards


    def listeCardsforHumans(self, lastCardPlayed):
        """
        Affiche les cartes du joueur dans un format lisible par l'homme.

        Args:
            lastCardPlayed: La dernière carte jouée dans la partie.
        """

        for index, card in enumerate(self.get_hand()):
            if (card.value >= lastCardPlayed.value):
                index = '[' + str(index) + ']'
                print(index, card, end=" ")


class PlayerAI(Player):
    def __init__(self, name=""):
        """
        Initialise une nouvelle instance de PlayerAI.

        Args:
            name: Le nom du joueur. Si aucun nom n'est fourni,
                  un nom aléatoire sera généré.
        """
        Player.__init__(self, name)

    def play_AI(self,lastCardPlayed):
        """
        Fait jouer l'IA en choisissant la carte la plus élevée possible.

        Args:
            lastCardPlayed: La dernière carte jouée dans la partie.

        Returns:
            La dernière carte jouée par l'IA.
        """

        highestCard = None
        for card in self.get_hand():
            if(card.value >= lastCardPlayed.value):
                highestCard = card
                print("Le joueur "+self.name+" a joué la carte : ", card)
                lastCardPlayed = card
                self.play(card)
                break
        if(highestCard == None):
            print("Le joueur "+self.name+" passe son tour !")
        return lastCardPlayed
        
    def play_AI_Random(self):
        """
        Fait jouer l'IA en choisissant une carte au hasard.

        Returns:
            La carte jouée par l'IA.
        """

        randomIndex = random.randint(0, len(self.hand)-1)
        randomCard = self.hand[randomIndex]
        self.hand.remove(randomCard)
        return randomCard