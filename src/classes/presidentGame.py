import Player
import Deck
import utils


class presidentGame:
    def __init__(self, players=None):
        """
        Initialise une partie de Président.

            Arguments:
            players -- une liste optionnelle de joueurs; si aucune liste n'est fournie, des joueurs aléatoires sont générés
        """

        # Utilisé pour les tests unitaires car la classe ne se fait reinstancier à chaque test avec les mêmes joueurs
        if (players == None):
            players = []

        if (len(players) < 3):
            # Generate random players
            for i in range(0, 3):
                players.append(Player.Player())
        self.players = players
        self.deck = Deck.Deck(52)
        self.deck.shuffle()
        self.distribute_cards()

    # Distribue les cartes au joueurs

    def distribute_cards(self):
        """
        Distribue un nombre égal de cartes à chaque joueur.
        """

        for player in self.players:
            # On distribue un nombres de cartes égales a la taille du paquet de cartes divisé par le nombre de joueurs
            deckLength = self.deck.get_size()
            for i in range(0, deckLength//len(self.players)):
                player.add_to_hand(self.deck.cards.pop())

    def checkWinner(self):
        """
        Vérifie si un joueur a gagné la partie.

        Returns:
            True si un joueur a gagné, False sinon.
        """

        for player in self.players:
            if (len(player.get_hand()) == 0):
                print("Le joueur "+player.name+" a gagné !")
                return True
        return False

    def checkIfPlayersCantPlay(self, lastCardPlayed):
        """
        Vérifie si au moins un joueur peut jouer une carte supérieure à la dernière carte jouée.
        Renvoie False si au moins un joueur peut jouer une carte supérieure, True si aucun joueur ne peut.

        Arguments:
        lastCardPlayed -- la dernière carte jouée
        """

        #  check if at least one player have a higher card than the last card played
        for player in self.players:
            for card in player.get_hand():
                if (card.value >= lastCardPlayed.value):
                    return False
        return True

    def has_queen_of_heart(self):
        """
        Vérifie si un joueur a la dame de cœur.

        Returns:
            Le joueur qui a la dame de cœur, False sinon.
        """
        
        for player in self.players:
            if (player.has_card("Dame", "coeur")):
                return player
        return False

    def shuffle_deck(self):
        """
        Mélange le paquet de cartes.

        Returns:
            Le paquet de cartes mélangé.
        """
        self.deck.shuffle()
