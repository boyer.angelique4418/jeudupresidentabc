import sys
import random

# Ajoute le dossier classes au path
sys.path.append('classes')

from classes import presidentGame
from classes import Player
# On crée les joueurs
player1 = Player.Player("Player 1")
player2 = Player.PlayerAI("Player 2 AI")
player3 = Player.PlayerAI("Player 3 AI")
player4 = Player.PlayerAI("Player 4 AI")


TheGame = presidentGame.presidentGame([player1, player2, player3, player4])

# Le jeux fais 52 cartes / 3 cela fais 17.3 cartes par joueur donc il faut donner la derniere carte a un joueur aléatoire
if(len(TheGame.deck.cards)):
    lastCard = TheGame.deck.cards.pop()
    randomPlayer = random.randint(0, len(TheGame.players)-1)
    TheGame.players[randomPlayer].add_to_hand(lastCard)
    print("Une carte a été ajouté au joueur "+TheGame.players[randomPlayer].name+".")

# On cherche si un joueur a la dame de coeur
playerPlaying = TheGame.has_queen_of_heart()
    
playerTurnIndex = TheGame.players.index(playerPlaying)

print(" Le joueur "+playerPlaying.name+" a la dame de coeur !")
print(" ")

# Début premier tour

# Si le premier joueur est un joueur humain ou AI
if (type(playerPlaying) == Player.Player):
    print("Voici les cartes que vous pouvez jouer :")

    # On affiche les cartes que le joueur peut jouer
    PlayableCard = playerPlaying.get_hand()

    # On affiche les cartes de facons lisibles Ex: [0] As de coeur [1] valet de trèfle etc...
    for index, card in enumerate(PlayableCard):
        index = '[' + str(index) + ']'
        print(index, card, end=" ")
    print(" ")

    while True:
        try:
            # On demande a l'utilisateur de choisir une carte
            cardIndex = int(input("Quelle carte voulez-vous jouer ? "))
            print(" ")

            # On récupere la carte que le joueur veut jouer
            card = PlayableCard[cardIndex]
            print("Vous avez joué la carte : ", card)

            # On joue la carte
            playerPlaying.play(card)
            lastCardPlayed = card
            break
        # Si l'utilisateur rentre une valeur qui n'est pas un entier
        except ValueError:
            input(
                "Vous n'avez pas entré un nombre valide.")
        # Si l'utilisateur rentre un entier qui n'est pas dans la liste des cartes jouables
        except IndexError:
            input(
                "Vous n'avez pas entré un nombre valide.")
else:
    # Si le joueur est un joueur AI
    lastCardPlayed = playerPlaying.play_AI_Random()
    print("Le joueur "+playerPlaying.name +
          " a joué la carte : ", lastCardPlayed)

# Fin premier tour

#  On passe au joueur suivant
playerTurnIndex = (playerTurnIndex + 1) % len(TheGame.players)

# Debut du jeu en boucle
while True:

    # On récupere le joueur qui doit jouer
    playerPlaying = TheGame.players[playerTurnIndex]

    input("C'est le tour du joueur "+playerPlaying.name +
          " , appuyez sur entrée pour continuer")

    # Si le joueur est un joueur humain
    if (type(playerPlaying) == Player.Player):
        print("Voici les cartes que vous pouvez jouer :")

        # On affiche les cartes que le joueur peut jouer
        PlayableCard = playerPlaying.getPlayableCard(lastCardPlayed)

        for index, card in enumerate(PlayableCard):
            # On affiche les cartes de facons lisibles Ex: [0] As de coeur
            index = '[' + str(index) + ']'
            print(index, card, end=" ")
        print("[20] Passer son tour")
        print(" ")

        while True:
            try:
                # On demande a l'utilisateur de choisir une carte
                cardIndex = int(input("Quelle carte voulez-vous jouer ? "))
                print(" ")

                # Si l'utilisateur a choisi de passer son tour
                if (cardIndex == 20):
                    print("Vous passez votre tour !")
                    break

                # On récupere la carte que le joueur veut jouer
                card = PlayableCard[cardIndex]
                print("Vous avez joué la carte : ", card)

                # On joue la carte
                playerPlaying.play(card)
                lastCardPlayed = card
                break
            # Si l'utilisateur rentre une valeur qui n'est pas un entier
            except ValueError:
                input(
                    "Vous n'avez pas entré un nombre valide. Ecrivez 20 si vous voulez passer votre tour")
            # Si l'utilisateur rentre un entier qui n'est pas dans la liste des cartes jouables
            except IndexError:
                input(
                    "Vous n'avez pas entré un nombre valide. Ecrivez 20 si vous voulez passer votre tour")
        print(" ")

    # Si le joueur est une IA
    else:
        beforePlayed = lastCardPlayed
        if (lastCardPlayed == "Nouveau Tour"):
            lastCardPlayed = playerPlaying.play_AI_Random()

        else:
            lastCardPlayed = playerPlaying.play_AI(lastCardPlayed)

        # Si l'IA ne peut pas jouer de carte elle passe son tour
        if (lastCardPlayed == None):
            lastCardPlayed = beforePlayed

    print("La derniere carte jouée est : ", lastCardPlayed)

    if (TheGame.checkWinner()):
        break
    # Verification si les joueurs ont encore des cartes a jouer
     # On donne le nombre de carte restante a chaque joueur
    print(" ")
    for player in TheGame.players:
        print("Le joueur "+player.name+" a " +
              str(len(player.get_hand()))+" cartes")
    print(" ")

    if (TheGame.checkIfPlayersCantPlay(lastCardPlayed)):
        print(" ")
        print("Personne ne peut jouer !")
        print("De nouveau le joueur " + TheGame.players[playerTurnIndex].name)
        lastCardPlayed = "Nouveau Tour"
    else:
        playerTurnIndex = (playerTurnIndex + 1) % len(TheGame.players)
    print(" ")
