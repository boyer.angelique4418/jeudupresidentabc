import sys
# Ajoute le dossier classes au path
sys.path.append('classes')
import unittest
from classes import Cartes
from classes import Deck
from classes import presidentGame


class TestCardsExercice1(unittest.TestCase):
    def test_Victory(self):
        """
        Test qui vient vérifier si un joueur a gagné
        """

        newgame = presidentGame.presidentGame()
        newgame.players[0].hand = []
        self.assertTrue(newgame.checkWinner())

    def test_has_queen_of_heart(self):
        """
        Vérifie qu'un joueur a bien la dame de coeur
        """
        newgame = presidentGame.presidentGame()
        playerHasQueen = newgame.has_queen_of_heart()
        self.assertTrue(playerHasQueen)


if __name__ == '__main__':
    unittest.main()