import sys
# Ajoute le dossier classes au path
sys.path.append('classes')
import unittest
from classes import Cartes
from classes import Deck



class TestCardsExercice1(unittest.TestCase):
    def test_card_constructor(self):
        self.assertTrue(isinstance(Cartes.Carte('As', 'coeur'), Cartes.Carte))

    def test_cards_equal_value(self):
        ace_of_hearts = Cartes.Carte('As', 'coeur')
        ace_of_spades = Cartes.Carte('As', 'pique')
        self.assertEqual(ace_of_hearts.value, ace_of_spades.value, 'Two cards having '
                                                       'same value should be considered equal')

    def test_cards_comparison(self):
        ace_of_hearts = Cartes.Carte('As', 'coeur')
        two_of_hearts = Cartes.Carte('2', 'coeur')
        five_of_hearts = Cartes.Carte('5', 'coeur')

        self.assertTrue(ace_of_hearts.value > five_of_hearts.value)
        self.assertTrue(two_of_hearts.value > ace_of_hearts.value > five_of_hearts.value,
                        'The two card is the highest card')
        self.assertTrue(five_of_hearts.value < two_of_hearts.value,
                        'The two card is the highest card')

class TestDeckExercice1(unittest.TestCase):
     def test_deck_has_52_cards(self):
         deck = Deck.Deck(52)
         self.assertEqual(len(deck.cards), 52, 'The president is a card game '
                                               'requiring 52 cards')
     def test_deck_shuffling(self):
         deck_1 = Deck.Deck(52)
         deck_2 = Deck.Deck(52)
         for i in range(0, 52):
             self.assertEqual(deck_1.cards[i].value, deck_2.cards[i].value, 'A new deck should not be automatically shuffled')
             
         # show the difference between the two decks
         deck_2.shuffle()
         # Compare those deck 
         self.assertNotEqual(deck_1.cards, deck_2.cards, 'A new deck should not be automatically shuffled')
                
                            

if __name__ == '__main__':
    unittest.main()
