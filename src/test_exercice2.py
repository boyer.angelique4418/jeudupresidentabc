import sys
# Ajoute le dossier classes au path
sys.path.append('classes')


import unittest
from classes import presidentGame
from classes import Player

class TestCardsExercice2(unittest.TestCase):
    def test_player_constructor(self):
        player_trump = Player.Player('Trump')
        self.assertTrue(player_trump.name == 'Trump')

    def test_incognito_player_should_have_random_name(self):
        player_incognito = Player.Player()
        self.assertFalse(player_incognito.name == '')

    def test_default_game_has_three_players(self):
        game = presidentGame.presidentGame()
        # print len of hand of each player
        self.assertTrue(len(game.players) == 3)

    def test_game_launch_distributes_cards(self):
        newgame = presidentGame.presidentGame()
        player_1 = newgame.players[0]
        player_2 = newgame.players[1]

        self.assertTrue(len(player_1.hand) > 0)
        self.assertTrue(len(player_1.hand) >= len(player_2.hand))

if __name__ == '__main__':
    unittest.main()

